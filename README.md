## full_zirconia-user 12 SQ3A.220705.003.A1 eng.sahil.20230504.054718 release-keys
- Manufacturer: teracube
- Platform: mt6765
- Codename: Teracube_2e
- Brand: Teracube
- Flavor: full_zirconia-user
- Release Version: 12
- Kernel Version: 4.19.127
- Id: SQ3A.220705.003.A1
- Incremental: eng.sahil.20230504.054718
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Teracube/Teracube_2e/Teracube_2e:11/RP1A.200720.011/202111011925:user/release-keys
- OTA version: 
- Branch: full_zirconia-user-12-SQ3A.220705.003.A1-eng.sahil.20230504.054718-release-keys
- Repo: teracube_teracube_2e_dump_17698
