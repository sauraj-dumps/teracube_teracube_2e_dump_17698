#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from Teracube_2e device
$(call inherit-product, device/teracube/Teracube_2e/device.mk)

PRODUCT_DEVICE := Teracube_2e
PRODUCT_NAME := lineage_Teracube_2e
PRODUCT_BRAND := Teracube
PRODUCT_MODEL := Teracube_2e
PRODUCT_MANUFACTURER := teracube

PRODUCT_GMS_CLIENTID_BASE := android-teracube

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="full_zirconia-user 12 SQ3A.220705.003.A1 eng.sahil.20230504.054718 release-keys"

BUILD_FINGERPRINT := Teracube/Teracube_2e/Teracube_2e:11/RP1A.200720.011/202111011925:user/release-keys
